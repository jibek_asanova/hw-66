import React from 'react';
import {NavLink} from "react-router-dom";

const Navbar = () => {
    return (
        <div className="Menu">
            <nav className="navbar navbar-light" style={{backgroundColor: '#e3f2fd'}}>
                <nav className="navbar navbar-expand-lg navbar-light ">
                    <div className="container-fluid">
                        <NavLink exact to="/pages/home" className="navbar-brand">My blog</NavLink>

                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"/>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div className="navbar-nav">
                                <NavLink exact to="/pages/home" className="nav-link" >Home</NavLink>
                                <NavLink to="/pages/about" className="nav-link" >About</NavLink>
                                <NavLink to="/pages/divisions" className="nav-link" >Divisions</NavLink>
                                <NavLink to="/pages/contacts" className="nav-link" >Contact Us</NavLink>
                                <NavLink to="/pages/admin" className="nav-link" >Admin</NavLink>
                            </div>
                        </div>
                    </div>
                </nav>
            </nav>
        </div>
    );
};

export default Navbar;