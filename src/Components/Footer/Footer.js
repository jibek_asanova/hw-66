import React from 'react';
import './Footer.css';

const Footer = () => {
    return (
        <div className="footer">
            <div className="container">
                <p>© 2015 portfolio landing page by Jibek Asanova</p>
            </div>
        </div>
    );
};

export default Footer;