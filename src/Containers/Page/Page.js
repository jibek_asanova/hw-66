import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import './Page.css';
import withLoader from "../../hoc/withLoader";

const Page = ({match, loading}) => {
    const [pageContent, setPageContent] = useState({
        title: '',
        content: ''
    });

    const pageId = match.params.page;

    useEffect(() => {
        const fetchData = async () => {
            if(pageId) {
                const response = await axiosApi.get('/pages/' + pageId + '.json');
                setPageContent(response.data);
            } else {
                const response = await axiosApi.get('/pages/home.json');
                setPageContent(response.data);
            }
        }
        fetchData().catch(console.error);
    }, [pageId]);

    return (
        loading ? null :
        <div className="PageContent">
            <h2>{pageContent.title}</h2>
            <p>{pageContent.content}</p>
        </div>
    );
};

export default withLoader(Page, axiosApi);