import React, {useEffect, useState} from 'react';
import './EditPage.css';
import axiosApi from "../../axiosApi";
import withLoader from "../../hoc/withLoader";

const EditPage = ({history, loading}) => {
    const [editPage, setEditPage] = useState({
        title: '',
        content: ''
    });

    const [pageSelect, setPageSelect] = useState({
        category: 'home'
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setPageSelect(prev => ({
            ...prev,
            [name]: value
        }));

        setEditPage(prev => ({
            ...prev,
            [name]: value
        }))
    };

    useEffect(() => {
        const fetchData = async () => {
            if(pageSelect.category) {
                const response = await axiosApi.get('/pages/' + pageSelect.category + '.json');
                setEditPage(response.data);
            }
        };

        fetchData().catch(console.error);
    }, [pageSelect.category]);

    const changePage = (e) => {
        e.preventDefault();
        const fetchData = async () => {
            await axiosApi.put('/pages/' + pageSelect.category + '.json', {
                title: editPage.title,
                content: editPage.content,
            });
            history.replace('/pages/' + pageSelect.category);
        };
        fetchData().catch(console.error);
    }

    return (
        loading ? null :
        <form className="Form" onSubmit={changePage}>
            <label htmlFor="cars">Page:</label>

            <select name="category" onChange={onInputChange} className="Input">
                <option value="home">Home</option>
                <option value="about">About</option>
                <option value="divisions">Divisions</option>
                <option value="contacts">Contacts</option>
            </select>
            <input
                className="Input"
                type="text"
                name="title"
                placeholder="Title"
                value={editPage.title}
                onChange={onInputChange}
            />
            <textarea
                className="Input TextArea"
                name="content"
                placeholder="Content"
                value={editPage.content}
                onChange={onInputChange}
            />
            <button type="submit" className="btn btn-primary">Save</button>
        </form>
    );
};

export default withLoader(EditPage, axiosApi);