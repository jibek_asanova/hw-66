import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./Components/Layout/Layout";
import Page from "./Containers/Page/Page";
import EditPage from "./Containers/EditPage/EditPage";

const App = () => (
    <BrowserRouter>
        <Layout>
            <Switch>
                <Route path="/" exact component={Page}/>
                <Route path="/pages/admin" component={EditPage}/>
                <Route path="/pages/:page" component={Page}/>
            </Switch>
        </Layout>
    </BrowserRouter>
)

export default App;
