import React, {useEffect, useMemo, useState} from 'react';
import Spinner from "../Components/UI/Spinner/Spinner";

const withLoader = (WrappedComponent, axios) => {

    return function LoaderHOC(props) {
        const [loading, setLoading] = useState(false);

        const icIdRequest = useMemo(() => {
            return axios.interceptors.request.use(function (req) {
                setLoading(true);
                return req;
            }, function (error) {
                return Promise.reject(error);
            });
        }, []);

        const icIdResponse = useMemo(() => {
            return axios.interceptors.response.use(function (res) {
                setLoading(false);
                return res;
            }, function (error) {
                return Promise.reject(error);
            });
        }, []);

        useEffect(() => {
            return () => {
                axios.interceptors.request.eject(icIdRequest);
                axios.interceptors.response.eject(icIdResponse);
            }
        }, [icIdRequest, icIdResponse]);


        return (
            <>
                {loading ? <Spinner/> : null}
                <WrappedComponent loading={loading} {...props}/>
            </>
        );
    };
};

export default withLoader;